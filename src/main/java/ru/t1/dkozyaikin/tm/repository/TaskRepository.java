package ru.t1.dkozyaikin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozyaikin.tm.api.repository.ITaskRepository;
import ru.t1.dkozyaikin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Task create(@NotNull String userId, @NotNull String name) {
        @NotNull Task task = new Task(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) {
        @NotNull Task task = new Task(name, description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public List<Task> findAllTasksByProjectId(@NotNull String userId, @NotNull final String projectId) {
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

}
