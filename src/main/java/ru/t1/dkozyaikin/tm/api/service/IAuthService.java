package ru.t1.dkozyaikin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException;

    void login(@Nullable String login, @Nullable String password) throws AbstractException;

    void logout();

    @NotNull
    Boolean isAuth();

    @NotNull
    String getUserId() throws AbstractException;

    @NotNull
    User getUser() throws AbstractException;

    void checkRoles(@Nullable Role[] roles) throws AbstractException;

}
