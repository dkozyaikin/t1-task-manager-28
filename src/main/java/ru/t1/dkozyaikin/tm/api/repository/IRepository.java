package ru.t1.dkozyaikin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator comparator);

    @NotNull
    Integer getSize();

    @Nullable
    M add(M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    boolean existsById(String id);

    @Nullable
    M findOneById(@Nullable String id) throws AbstractException;

    @Nullable
    M findOneByIndex(@Nullable Integer index) throws AbstractException;

    void clear();

    @Nullable
    M remove(M model);

    @Nullable
    M removeById(String id) throws AbstractException;

    @Nullable
    M removeByIndex(Integer index) throws AbstractException;

}
