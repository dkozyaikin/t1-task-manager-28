package ru.t1.dkozyaikin.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataJsonFasterXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-json-fasterxml-load";

    @NotNull
    public static final String DESCRIPTION = "Loads from json by FasterXml";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[JSON FASTERXML LOAD]");
        final byte[] bytes = Files.readAllBytes(Paths.get(FILE_FASTERXML_JSON));
        @NotNull final String json = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
