package ru.t1.dkozyaikin.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.dto.Domain;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public final class DataYamlSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-yaml-save";

    @NotNull
    public static final String DESCRIPTION = "Saves to yaml file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[YAML SAVE]");
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_YAML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final String yaml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(yaml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
